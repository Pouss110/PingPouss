package com.monnoye.game.PingPouss.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.monnoye.game.PingPouss.PingPouss;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		config.useGLSurfaceView20API18=true;
		config.useAccelerometer=false;
		config.useCompass=false;
		config.useWakelock=true;
		initialize(new PingPouss(), config);
	}
}
