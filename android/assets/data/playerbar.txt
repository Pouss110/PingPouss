playerBar.png
format: RGBA8888
filter: Linear,Linear
repeat: none
barG1L
  rotate: false
  xy: 2, 2
  size: 100, 20
  orig: 100, 20
  offset: 0, 0
  index: -1
barG2L
  rotate: false
  xy: 104, 2
  size: 100, 20
  orig: 100, 20
  offset: 0, 0
  index: -1
barGreen
  rotate: false
  xy: 206, 2
  size: 100, 20
  orig: 100, 20
  offset: 0, 0
  index: -1
barRed
  rotate: false
  xy: 308, 2
  size: 100, 20
  orig: 100, 20
  offset: 0, 0
  index: -1
