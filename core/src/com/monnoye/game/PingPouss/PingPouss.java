package com.monnoye.game.PingPouss;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.monnoye.game.PingPouss.Logic.Profile;
import com.monnoye.game.PingPouss.Screen.gameDrawScreen;
import com.monnoye.game.PingPouss.Screen.gameLaunchScreen;
import com.monnoye.game.PingPouss.Screen.gameMenuScreen;
import com.monnoye.game.PingPouss.Screen.gameOverScreen;
import com.monnoye.game.PingPouss.Screen.gameScoreScreen;
import com.monnoye.game.PingPouss.Screen.gameScreen;
import com.monnoye.game.PingPouss.Screen.gameScreenCass;
import com.monnoye.game.PingPouss.Screen.gameWhackemolCass;

public class PingPouss extends Game {
	private FPSLogger fps;
	private SpriteBatch batch;
	private Skin skin;
	
	public Preferences prefs;
	
	public gameScreen gameScreen;
	public gameScreenCass gameScreenCass;
	public gameOverScreen gameOverScreen;
	public gameLaunchScreen gameLaunchScreen;
	public gameDrawScreen gameDrawScreen;
	public gameMenuScreen gameMenuScreen;
	public gameScoreScreen gameScoreScreen;
	public gameWhackemolCass gameWhakemolScreen;
	@Override
	public void create () {
		fps = new FPSLogger();
		batch = new SpriteBatch();
		//Assets.load();
		Profile.load();
		Assets.load();
		gameScreen = new gameScreen(this);
		gameWhakemolScreen = new gameWhackemolCass(this);
		gameScoreScreen = new gameScoreScreen(this);
		gameScreenCass = new gameScreenCass(this); 
		gameOverScreen = new gameOverScreen(this);
		gameLaunchScreen = new gameLaunchScreen(this); 
		gameDrawScreen = new gameDrawScreen(this);
		gameMenuScreen = new gameMenuScreen(this);
		setScreen(gameLaunchScreen);
	}
	
	public Skin getSkin(){return skin;}
	public SpriteBatch getBatch() {return batch;}
	
	public void dispose() {
		//remember dispose the current screen
		Assets.dispose();
		getScreen().dispose();
		batch.dispose();
		super.dispose();
		//Use this function to print stuff to the console
		Gdx.app.log("PingPouss", "Disposed");
	}
	
	@Override
	public void render () {
		super.render();
		//fps.log();
		//System.out.print();
	}
}
