package com.monnoye.game.PingPouss.Object;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class Entity extends Actor{
	//common things that all moving entities will need in our game
	public Vector2 position;
	public Vector2 rotation;
	public Vector2 velocity;
	
	public Entity (float x, float y) {
		this.position = new Vector2(x,y);
		this.rotation = new Vector2();
		this.velocity = new Vector2();
	}
	
	//method to update object position
	public void update() {
		position.add(velocity);
	}
	
	public void rotate(float x, float y) {
		rotation.set(position);
		rotation.sub(x, y);
		
	}
	public void path(float x, float y){
		
	}
}
