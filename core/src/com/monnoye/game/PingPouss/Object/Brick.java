package com.monnoye.game.PingPouss.Object;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.monnoye.game.PingPouss.Assets;


public class Brick extends Actor{
	public Vector2 position;
	public Vector2 rotation;
	
	public Sprite bsprite;
	
	public Rectangle bounds;
	public int bricTint, bricFon;
	
		
	public Brick(float x, float y, int tint, int fonc) {
		this.position = new Vector2(x, y);
		this.rotation = new Vector2();
		this.bsprite = new Sprite(Assets.brickTex);
		
		
		this.bounds = new Rectangle(x-(bsprite.getWidth()/2), y-(bsprite.getHeight()/2), bsprite.getWidth(), bsprite.getHeight());
		bricTint = tint;
		bricFon = fonc;
		
	}

	public void rotate(float x, float y) {
		rotation.set(position);
		
		rotation.sub(x, y);
		
	}
	
	public void draw(Batch batch, float alpha){
		batch.draw(bsprite, this.position.x, this.position.y);
	}

	

}
