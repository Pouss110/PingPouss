package com.monnoye.game.PingPouss.Object;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

public class Bonus extends Entity{
		public int Type;
	public Bonus(float x, float y, int type) {
		super(x, y);
		Type = type;
	}
	
	public void update(int x, int y){
		velocity.set(x, y);
		position.add(velocity);
	}
	
	
}
