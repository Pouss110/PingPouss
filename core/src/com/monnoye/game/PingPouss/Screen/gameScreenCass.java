package com.monnoye.game.PingPouss.Screen;

import java.util.ArrayList;
import java.util.Iterator;

//import org.json.JSONArray;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.monnoye.game.PingPouss.Assets;
import com.monnoye.game.PingPouss.PingPouss;
import com.monnoye.game.PingPouss.Logic.InputHandler;
import com.monnoye.game.PingPouss.Logic.MyGestureListener;
import com.monnoye.game.PingPouss.Logic.Profile;
import com.monnoye.game.PingPouss.Object.Ball;
import com.monnoye.game.PingPouss.Object.Block;
import com.monnoye.game.PingPouss.Object.Bomb;
import com.monnoye.game.PingPouss.Object.Bonus;
import com.monnoye.game.PingPouss.Object.Brick;
import com.monnoye.game.PingPouss.Object.LevelFactory;
import com.monnoye.game.PingPouss.Object.Player;

public class gameScreenCass implements Screen {
	ShapeRenderer shaperender;
	float w, h;
	OrthographicCamera camera;
	SpriteBatch batch;
	float ballSpeedY;
	float ballSpeedX;
	float bsY, bsX;
	public static float temps;
	private static final float PAUSE_TIME = 500f;
	float time = 0;
	float circleBposX, circleBposY;
	Json json;
	Gson gson;
	LevelFactory anim;
	Stage stage;
	Skin skin;
	public static Label scoretext;
	public static ProgressBar pBar;
	Rectangle playerBarBounds, ballBounds, bonusBounds, barBounds;
	Sprite playerSprite, ballSprite, brickSprite, bonusSprite, blockSprite,
			bar, circleB;
	public static int nbrLife;
	public static int level;
	int testIntW;
	int testIntH;
	int tailleBar;
	int index;
	public static int score, scoreBar;
	int longueur, longueurMax;
	long beginTime, loseTime, delay;

	Animation explosionAnim;
	//JSONArray jTokener;
	TextureRegion[] regionInitial;
	CharSequence strBegin = "Touchez pour démarrer!";
	CharSequence strEnd = "Game Over...";
	CharSequence strNewlife = "Rahh, allez réessaye!";
	public static CharSequence strScore = "";

	public static int i = 0;
	public static boolean ballStop = true;
	public static int mode = 0;
	Player player;
	Ball ball;
	Texture playerTexture, player2LT, player1LT, bonusTexture,
			bonusLifeTexture, playerLoseT, blocTex, pauseButTex, newPlayerTex,
			circleBonus, explosion;
	Sprite exploSprite;
	TextureRegion exploCurrent;

	PingPouss game;

	private ArrayList<Brick> brick;

	Iterator<Brick> brickIterator;

	ArrayList<Bonus> bonusList;
	Iterator<Bonus> bonusIterator;
	public Window windowWin;
	public static ArrayList<Bomb> bombeList;
	Iterator<Bomb> bombeIter;
	Bomb bombe;
	public static ArrayList<Block> blockList;
	public static Array<Vector2> path;
	public static Vector2 debutVec;
	public static Vector2 finVec;
	public static boolean touchUp = false;
	public static boolean tap = false;
	boolean explay = false;
	Rectangle rectPlay;
	Iterator<Block> blockIter;
	Pixmap newPlayBar, circlePixmap;
	Brick b;
	Bonus bonus;
	Block block, lastBlock;
	private PlayMode playMode;

	public gameScreenCass(final PingPouss game) {
		this.game = game;

		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();

		path = new Array<Vector2>();

		rectPlay = new Rectangle();
		debutVec = new Vector2();
		finVec = new Vector2();
		camera.setToOrtho(true, w, h);
		camera.update();
		// parametre de la bar trac�
		newPlayBar = new Pixmap(64, 64, Format.RGBA8888);
		newPlayBar.setColor(0.2f, 0.2f, 0.2f, 0.8f);
		newPlayBar.fillRectangle(0, 0, 100, 10);

		newPlayerTex = new Texture(newPlayBar);
		// cercle de destruction en mode bonus
		circlePixmap = new Pixmap(64, 64, Format.RGBA8888);
		circlePixmap.setColor(Color.RED);
		circlePixmap.fillCircle(25, 25, 25);

		circleBonus = new Texture(circlePixmap);

		// configure the UI
		stage = new Stage(new ScreenViewport());
		skin = Assets.skin;

		strScore = new String().valueOf(score);

		scoretext = new Label(strScore, skin, "default", Color.WHITE);
		// scoretext.setColor(Color.RED);

		pauseButTex = new Texture(Gdx.files.internal("sheets/pauseButton.png"));
		pauseButTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		TextureRegion pause = new TextureRegion(pauseButTex);

		ProgressBarStyle pbStyle = new ProgressBarStyle(
				new TextureRegionDrawable(new TextureRegion(new Texture(
						Gdx.files.internal("sheets/progressB.png")))),
				new TextureRegionDrawable(new TextureRegion(new Texture(
						Gdx.files.internal("sheets/bonusLife3d.png")))));
		pbStyle.knobBefore = new TextureRegionDrawable(new TextureRegion(
				new Texture(Gdx.files.internal("sheets/progressBplein.png"))));
		pBar = new ProgressBar(0, 100, 2, true, pbStyle);

		ImageButtonStyle imgButStyle = new ImageButtonStyle();
		imgButStyle.imageUp = new TextureRegionDrawable(pause);

		Table table = new Table();
		table.setFillParent(true);
		stage.addActor(table);

		ImageButton pauseB = new ImageButton(imgButStyle);

		TextButton repriseButt = new TextButton("Touch Me", skin);
		TextButton quitterButt = new TextButton("Quitter", skin);
		TextButton winBut = new TextButton("Menu", skin);
		TextButton nextLevel = new TextButton("Suivant", skin);

		TextButton life1 = new TextButton("", skin);
		TextButton life2 = new TextButton("", skin);
		TextButton life3 = new TextButton("", skin);

		final Window window = new Window("Pause", skin);

		window.setSize(w / 2, h / 3 + 30);
		window.setPosition(w / 2 - window.getWidth() / 2,
				h / 2 - window.getHeight() / 2);

		// window.defaults().spaceBottom(10);

		window.add(repriseButt);
		window.row();
		window.add(quitterButt);
		// window.row().fill().expandX();

		window.isTouchable();
		stage.addActor(window);

		window.setVisible(false);

		windowWin = new Window("Felicitations", skin);
		windowWin.setSize(w / 2, h / 3 + 30);
		windowWin.setPosition(w / 2 - window.getWidth() / 2,
				h / 2 - window.getHeight() / 2);
		windowWin.add(winBut);
		windowWin.row();
		windowWin.add(nextLevel);

		stage.addActor(windowWin);
		windowWin.setVisible(false);

		nextLevel.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("next level");

				Assets.resetScore();
				level++;
				addNewBrick();
				windowWin.setVisible(false);
			}
		});

		winBut.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("gagn�");
				windowWin.setVisible(false);
				Assets.resetScore();
				game.setScreen(game.gameMenuScreen);

			}
		});

		repriseButt.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("reprise");
				ball.velocity.x = bsX;
				ball.velocity.y = bsY;
				window.setVisible(false);
			}
		});

		quitterButt.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("quitter");
				window.setVisible(false);
				Assets.resetScore();
				game.setScreen(game.gameLaunchScreen);
			}
		});
		// pauseB

		pauseB.isTouchable();
		pauseB.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("pause");
				bsX = ball.velocity.x;
				bsY = ball.velocity.y;
				window.setVisible(true);
				ball.velocity.x = 0;
				ball.velocity.y = 0;

			}
		});
		table.add(scoretext).top().left().pad(5).height(0);
		table.add(pauseB).expand().top().right();
		table.row();
		table.add();
		table.add(pBar).expand().top().right();
		table.row();

		table.debug();

		// load asset
		batch = game.getBatch();

		blocTex = new Texture(Gdx.files.internal("sheets/barBlock.png"));
		blocTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		playerTexture = new Texture(Gdx.files.internal("sheets/barGreen.png"));
		playerTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		player2LT = new Texture(Gdx.files.internal("sheets/barG2L.png"));
		player2LT.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		player1LT = new Texture(Gdx.files.internal("sheets/barG1L.png"));
		player1LT.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		playerLoseT = new Texture(Gdx.files.internal("sheets/barRed.png"));
		playerLoseT.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		bonusLifeTexture = new Texture(
				Gdx.files.internal("sheets/bonusLife.png"));
		bonusLifeTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		player = new Player(w / 2, h - 20);
		ball = new Ball(w / 2, h - 38);
		playerBarBounds = new Rectangle();
		barBounds = new Rectangle();
		ballBounds = new Rectangle();
	
		bonusBounds = new Rectangle();
		bar = new Sprite();
		circleB = new Sprite();
		// Liste des brick
		// Vector2 bombe = new Vector2();
		brick = new ArrayList<Brick>();
		brick.add(new Brick(10, 10, 2, 2));
		bonusList = new ArrayList<Bonus>();
		blockList = new ArrayList<Block>();
		bombeList = new ArrayList<Bomb>();
		json = new Json();
		gson = new Gson();

		nbrLife = 3;

		tailleBar = 50;

		addNewBrick();
		// score = 0;
		beginTime = System.nanoTime();
		delay = 1000000000;
		// temps = 0;
	}

	private void addNewBrick() {
		mode = 0;
		int nbrLevelTotal;
		FileHandle file = Gdx.files.internal("data/level/");
		nbrLevelTotal = file.list().length;
		level = 0;

		if (level == 0) {
			brick.clear();
			// mur de briques
			testIntW = MathUtils.random(5, 5);
			testIntH = MathUtils.random(5, 6);
			for (int l = 0; l <= testIntH; l++) {
				for (int i = 0; i <= testIntW / 2; i++) {
					brick.add(new Brick(w / 2 - 15 - i * 30, h / 2 - l * 15,
							MathUtils.random(0, 5), 1));
					
				}
				for (int i = 0; i <= testIntW / 2; i++) {
					brick.add(new Brick(w / 2 + 15 + i * 30, h / 2 - l * 15,
							MathUtils.random(0, 5), 1));
				}
			}
			for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
				b = brickIterator.next();
					b.rotate(50, 20);
			
			
			
						}
			
		}

		if (level == 1 || level == 2) {
			for (int i = 1; i <= level; i++) {
				String gs = readFile("data/level/level0" + i + ".lvl");
				brick = gson.fromJson(gs, new TypeToken<ArrayList<Brick>>() {
				}.getType());
			}
		} else {
			for (int i = 1; i <= level; i++) {
				String gs = readFile("data/level/level" + i + ".lvl");
				brick = gson.fromJson(gs, new TypeToken<ArrayList<Brick>>() {
				}.getType());
			}

		}
		/*
		 * if (level == 2) { testIntW = MathUtils.random(1, 14); testIntH =
		 * MathUtils.random(26, 26); for (int l = 0; l <= testIntH; l++) { for
		 * (int i = 0; i <= testIntW / 2; i++) { brick.add(new Brick(w / 2 - 15
		 * - i * 30, h / 2 - l * 15, MathUtils.random(0, 5), 1)); } for (int i =
		 * 0; i <= testIntW / 2; i++) { brick.add(new Brick(w / 2 + 15 + i * 30,
		 * h / 2 - l * 15, MathUtils.random(0, 5), 1)); } } }
		 */

		/*
		 * if (level == 1) {
		 * 
		 * String gs = readFile("data/level0"+1+".lvl"); brick =
		 * gson.fromJson(gs, new TypeToken<ArrayList<Brick>>() { }.getType());
		 * 
		 * } if (level == 2) {
		 * 
		 * String gs = readFile("data/level02.lvl"); brick = gson.fromJson(gs,
		 * new TypeToken<ArrayList<Brick>>() { }.getType());
		 * 
		 * }
		 */

	}

	@Override
	public void render(float delta) {
		// System.out.print(playerTexture.getHeight()/playerTexture.getWidth());
		// clear the screen
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();

		exploSprite = new Sprite();

		brickSprite = new Sprite(Assets.brickReg[0][0]);
		circleB = new Sprite(circleBonus);
		ballSprite = new Sprite(Assets.ballTex);
		bonusSprite = new Sprite(bonusLifeTexture);
		blockSprite = new Sprite(blocTex);

		ballSprite.setSize(10, 10);

		if (nbrLife == 3) {
			// playerSprite.setTexture(playerTexture);
			playerSprite = new Sprite(playerTexture);

		}
		if (nbrLife == 2) {
			playerSprite.setTexture(player2LT);
			// playerSprite = new Sprite(player2LT);
		}
		if (nbrLife == 1) {
			playerSprite.setTexture(player1LT);
		}
		if (nbrLife == 0) {
			playerSprite.setTexture(playerLoseT);
		}

		float ratioPlayerBar = Math.abs(playerTexture.getWidth()
				/ playerTexture.getHeight());
		float ratioNPlayerBar = Math.abs(newPlayerTex.getWidth()
				/ newPlayerTex.getHeight());
		float ratioBrick = Math.abs(Assets.brickTex.getWidth()
				/ Assets.brickTex.getHeight());

		ball.update();
		player.update();

		// Draw stuff to the screen
		batch.setProjectionMatrix(camera.combined);
		batch.begin();

		// lancer de balle android

		if (Gdx.input.isTouched(1) == true) {
			ballStop = false;
		}

		if (i == 0) {
			if (ballStop == true) {
				ball.position.x = w / 2;
				ball.position.y = h - 30;
				ball.velocity.y = 0;
			} else if (ballStop == false) {
				// ball.velocity.y = -3;
				// ball.velocity.x = (Gdx.input.getX()/480)*5;

				i = 3;
			} else {

			}
		}
		ballSprite.setPosition(ball.position.x - (ballSprite.getWidth() / 2),
				ball.position.y - (ballSprite.getHeight() / 2));
		ballSprite.draw(batch);
		// batch.draw(Assets.ballTex, ball.position.x -
		// (Assets.ballTex.getWidth() / 2), ball.position.y -
		// (Assets.ballTex.getHeight() / 2), 8, 8);
		bonusSprite.setSize(50, 50);

		bonusSprite.flip(true, false);
		// System.out.println(playerSprite.getWidth());
		ballBounds.set(ball.position.x - (ballSprite.getWidth() / 2),
				ball.position.y - (ballSprite.getHeight() / 2),
				ballSprite.getWidth(), ballSprite.getHeight());

		// check for collision
		for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
			b = brickIterator.next();
			

			if (ballBounds.overlaps(b.bounds)) {
				if (MathUtils.random(0, 100) >= 50) {
					bonusList.add(new Bonus(b.position.x, b.position.y,
							MathUtils.random(1, 4)));
				}
				score += 1;

				scoreBar += 1;
				// change la valeur de la progress bar
				pBar.setValue(scoreBar);

				strScore = new String().valueOf(score * 10);
				scoretext.setText(strScore);
				// Supression de la brique
				// Brique MultiCouche
				// b.bricFon -=1;
				// if(b.bricFon == 1)
				brickIterator.remove();
				float penteBrick = Math.abs((ball.position.x - b.position.x)
						/ (ball.position.y - b.position.y));

				ballSpeedX = ball.velocity.x - 2 * ball.velocity.x;
				if (penteBrick <= ratioBrick) {
					ball.velocity.y = ball.velocity.y - 2 * ball.velocity.y;

				} else if (penteBrick >= ratioBrick) {

					ball.velocity.x = ballSpeedX;

				}

				break;
			}
		}

		for (bonusIterator = bonusList.iterator(); bonusIterator.hasNext();) {
			bonus = bonusIterator.next();
			bonusBounds.set(bonus.position.x - (bonusSprite.getWidth() / 2),
					bonus.position.y - (bonusSprite.getHeight() / 2),
					bonusSprite.getWidth(), bonusSprite.getHeight());
			bonus.update();

			if (bonusBounds.overlaps(barBounds)) {

				if (bonus.Type == 2) {
					if (nbrLife < 3)
						nbrLife++;

					bonusIterator.remove();
				} else if (bonus.Type == 3) {
					score += 10;
					strScore = new String().valueOf(score * 10);
					scoretext.setText(strScore);
					bonusIterator.remove();
				} else if (bonus.Type == 4) {
					scoreBar += 15;
					pBar.setValue(scoreBar);
					bonusIterator.remove();
				} else if (bonus.Type == 1) {

					bonusIterator.remove();

				}

			}
		}
		// dessiner les vies
		if (nbrLife == 3) {
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					65);
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					105);
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					145);
		} else if (nbrLife == 2) {
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 65);
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					105);
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					145);
		} else if (nbrLife == 1) {
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 65);
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 105);
			batch.draw(Assets.life, w - 20 - Assets.life.getRegionWidth() / 2,
					145);
		} else if (nbrLife == 0) {
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 65);
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 105);
			batch.draw(Assets.looselife, w - 20 - Assets.life.getRegionWidth()
					/ 2, 145);
		}

		// draw the bonus
		for (bonusIterator = bonusList.iterator(); bonusIterator.hasNext();) {
			bonus = bonusIterator.next();
			bonus.update();
			if (bonus.Type == 3) {
				bonusSprite.setTexture(Assets.brickTex);
				batch.draw(bonusSprite,
						bonus.position.x - (ballSprite.getWidth() / 2),
						bonus.position.y - (ballSprite.getHeight() / 2));
			}
			if (bonus.Type == 2) {
				bonusSprite.setTexture(Assets.butMenuTex);

				batch.draw(bonusSprite,
						bonus.position.x - (ballSprite.getWidth() / 2),
						bonus.position.y - (ballSprite.getHeight() / 2));
			}
			if (bonus.Type == 4) {
				bonusSprite.setTexture(Assets.ballTex);
				batch.draw(bonusSprite,
						bonus.position.x - (ballSprite.getWidth() / 2),
						bonus.position.y - (ballSprite.getHeight() / 2));
			}

			bonus.velocity.y = 2;
		}
		// draw the bricks
		for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
			b = brickIterator.next();

			
			//essai n1 pour alleger le gamescreencass
			//brickSprite.setRegion(Assets.brickReg[b.bricTint][b.bricFon]);
			batch.setColor(Color.YELLOW);
			
			
			
				
			
			
			batch.draw(b.bsprite,
					b.position.x - (b.bsprite.getWidth() / 2), b.position.y
							- (b.bsprite.getHeight() / 2));
			batch.setColor(Color.WHITE);
		}
		
		
		if (scoreBar > 5 & mode == 0) {
			mode = 1;
			bsX = ball.velocity.x;
			bsY = ball.velocity.y;

			ball.velocity.x = 0;
			ball.velocity.y = 0;
		}
		if (mode == 1 & scoreBar <= 0) {
			mode = 0;
			scoreBar = 0;

			ball.velocity.x = bsX;
			ball.velocity.y = bsY;
			MyGestureListener.xTap = 0;
		}
		if (ballBounds.overlaps(barBounds) && i == 3) {
			float ratioBar = Math.abs(barBounds.getWidth()
					/ barBounds.getHeight());
			System.out.println(ball.velocity.x);

			float pente = Math.abs((ball.position.x - player.position.x)
					/ (ball.position.y - player.position.y));
			if (pente <= ratioBar && ball.position.y < player.position.y) {
				ball.velocity.y = ball.velocity.y - 2 * ball.velocity.y;
				ball.velocity.x = ball.velocity.x
						- ((ball.position.x - (barBounds.x + (barBounds.width / 2))) * 0.004f);
			} else if (pente >= ratioBar) {
				ball.velocity.x = ball.velocity.x - 2 * ball.velocity.x;
				ball.velocity.y = ball.velocity.y - 2 * ball.velocity.y;
			}

			System.out
					.println((ball.position.x - (barBounds.x + (barBounds.width / 2))));
			// System.out.println(ball.velocity.x);

			// ball.velocity.y = ball.velocity.y - 2 * ball.velocity.y;

			debutVec.x = 0;
			finVec.x = 0;
			debutVec.y = 0;
			finVec.y = 0;

			barBounds.setPosition(0, 0);
			// Swipe ==>extra mode
			// if (mode == 1/*swipe during extra mode(bonus)*/)
			// {
			// tracer un trajet avec le doigt,
			// tirer a partir du sol,

			// }
		}
		// rebond de la balle sur le joueur
		/*
		 * if (ballBounds.overlaps(playerBarBounds) && i == 3) {
		 * 
		 * ballSpeedY = ball.velocity.y - 2 * ball.velocity.y; ballSpeedX =
		 * ball.velocity.x;
		 * 
		 * float pente = Math.abs((ball.position.x - player.position.x) /
		 * (ball.position.y - player.position.y)); // System.out.print(pente);
		 * if (pente <= ratioPlayerBar && ball.position.y < player.position.y) {
		 * ball.velocity.y = ballSpeedY; ball.velocity.x = (float)
		 * (ball.velocity.x - (player.position.x - ball.position.x) * 0.05); }
		 * else if (pente >= ratioPlayerBar) { ball.velocity.x = ball.velocity.x
		 * - 2 * ball.velocity.x;
		 * 
		 * }
		 * 
		 * }
		 */
		// collision joueur _mur
		if (player.position.x > w - (playerSprite.getWidth() / 2)) {
			player.position.x = player.position.x - 5;
			player.velocity.x = 0;
		}
		if (player.position.x < (playerSprite.getWidth() / 2)) {
			player.position.x = player.position.x + 5;
			player.velocity.x = 0;
		}
		// collision mur
		if (ball.position.x <= 0 && i == 3) {
			ball.velocity.x = ball.velocity.x - 2 * ball.velocity.x;
		} else if (ball.position.x >= w && i == 3) {
			ball.velocity.x = ball.velocity.x - 2 * ball.velocity.x;
		}
		if (ball.position.y <= 0 && i == 3) {
			ball.velocity.y = ball.velocity.y - 2 * ball.velocity.y;
		} else if (ball.position.y >= h + 20 && i == 3) {

			Gdx.input.vibrate(1000);
			nbrLife--;
			i = 0;
			ballStop = true;
			Timer.schedule(new Task() {

				@Override
				public void run() {

					ball.position.x = player.position.x;
					ball.position.y = player.position.y - 18;
					ball.velocity.x = 0;
					ball.velocity.y = 0;
					ballStop = true;

				}

			}, 2);

			if (score >= Profile.getHighScore(level)) {
				Profile.setHighScore(score * 10, level);
			}

		}

		if (touchUp == true && i == 3) {
			longueur = 0;
			longueurMax = Profile.getLongMax();
			if (debutVec.x >= finVec.x) {
				longueur = (int) (finVec.x - debutVec.x);

			} else {
				longueur = (int) (debutVec.x - finVec.x);

			}
			if (longueur < longueurMax)
				longueur = longueurMax;

			if (longueur < -25) {
				bar = new Sprite(newPlayerTex, longueur, 5);
				if (debutVec.y >= h / 2 || finVec.y >= h / 2) {
					if (debutVec.x >= finVec.x) {
						batch.draw(bar, finVec.x, finVec.y);
					} else {
						batch.draw(bar, debutVec.x, debutVec.y);
					}
				}
				if (debutVec.x >= finVec.x) {
					barBounds.set(finVec.x, finVec.y, bar.getWidth(), 5);
				} else {
					barBounds.set(debutVec.x, debutVec.y, bar.getWidth(), 5);
				}
				
				batch.draw(Assets.butMenuTex, barBounds.x, barBounds.y,
						barBounds.getWidth(), 5);
				player.position.x = barBounds.x + barBounds.getWidth() / 2;
				player.position.y = barBounds.y - 5 / 2;

			}
		}

		// timer pour le d�but
		if (i == 0 && nbrLife == 3) {

			if (System.nanoTime() - beginTime > 10 * 1000 * 1000000000) {

				Assets.font.setScale(1f, -1f);
				Assets.font.draw(batch, "Touch me", w / 3, h / 2);

			}

		}
		if (brick.isEmpty() == true)
			System.out.println("Plus de brick");

		// controle du niveau
		if (brick.isEmpty() == true && nbrLife > 0) {

			windowWin.setVisible(true);
			if (score >= Profile.getHighScore(level)) {
				Profile.setHighScore(score * 10, level);
				Assets.font.setScale(1f, -1f);

				Assets.font.draw(batch, "Nouveau Record !!", w / 5, h / 6);

			}

			if (Profile.getLevelMax() <= level) {

				Profile.setLevelMax(level + 1);
			}

			// game.setScreen(game.gameMenuScreen);

			// level++;
			// addNewBrick();
		}

		if (nbrLife == 0) {

			if (score * 10 >= Profile.getHighScore(level)) {
				Profile.setHighScore(score * 10, level);
			}
			Timer.schedule(new Task() {

				@Override
				public void run() {
					brick.clear();
					nbrLife = 3;
					i = 0;
					ballStop = true;
					game.setScreen(game.gameOverScreen);

				}

			}, 2);
		}

		if (bombeList.isEmpty()) {

		}

		if (mode == 1) {

			circleBposX = MyGestureListener.xTap;
			circleBposY = MyGestureListener.yTap;
			
			if (circleBposX != 0) {
				if (Gdx.input.justTouched()) {

				}
				bombeList.add(new Bomb(circleBposX,circleBposY));
				
				
				playAnim(circleBposX, circleBposY);

				if (explosionAnim.isAnimationFinished(temps)) {
					temps = 0;
					for (bombeIter = bombeList.iterator(); bombeIter.hasNext();) {
						bombe = bombeIter.next();
						
						
						bombeIter.remove();

					}
				}
			}

			for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
				b = brickIterator.next();
				
				Rectangle circleRect = new Rectangle(
						MyGestureListener.xTap - 25 / 2,
						MyGestureListener.yTap - 25 / 2, 25, 25);
				if (circleRect.overlaps(b.bounds)) {
					brickIterator.remove();
					scoreBar -= 1;
					score += 3;
					pBar.setValue(scoreBar);
					strScore = new String().valueOf(score * 10);
					scoretext.setText(strScore);
				}

				tap = false;
			}

		}

		batch.end();
		pBar.setSize(20, 300);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		Table.drawDebug(stage);

	}

	public void loadAnim() {
		explosion = new Texture(Gdx.files.internal("sheets/explosion.png"));
		TextureRegion[][] tmp = TextureRegion.split(explosion,
				explosion.getWidth() / 8, explosion.getHeight() / 8);
		regionInitial = new TextureRegion[4];

		index = 0;

		for (int i = 0; i < 4; i++) {
			regionInitial[index++] = tmp[0][i];
		}

		explosionAnim = new Animation(1f / 4, regionInitial);

		explosionAnim.setPlayMode(PlayMode.NORMAL);
	}

	public void playAnim(float x, float y) {
		// temps=0;

		explosion = new Texture(Gdx.files.internal("sheets/explosion.png"));
		TextureRegion[][] tmp = TextureRegion.split(explosion,
				explosion.getWidth() / 8, explosion.getHeight() / 8);
		regionInitial = new TextureRegion[6];

		index = 0;

		for (int i = 0; i < 6; i++) {
			regionInitial[index++] = tmp[0][i];
		}

		explosionAnim = new Animation(1f / 6, regionInitial);

		explosionAnim.setPlayMode(PlayMode.NORMAL);

		temps += Gdx.graphics.getDeltaTime();

		exploCurrent = explosionAnim.getKeyFrame(temps);

		batch.draw(exploCurrent, x - exploCurrent.getRegionWidth() / 2, y
				- exploCurrent.getRegionHeight() / 2, 50, 50);
	}

	public void stopAnim() {

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		InputMultiplexer iM = new InputMultiplexer();
		iM.addProcessor(stage);
		iM.addProcessor(new InputHandler(player, ball, block));

		iM.addProcessor(new GestureDetector(new MyGestureListener(ball)));
		addNewBrick();

		beginTime = System.nanoTime();
		Gdx.input.setInputProcessor(iM);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	public static void writeFile(String fileName, String s) {
		FileHandle file = Gdx.files.local(fileName);
		file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s),
				false);
	}

	public static String readFile(String fileName) {
		FileHandle file = Gdx.files.internal(fileName);
		if (file != null && file.exists()) {
			String s = file.readString();
			if (!s.isEmpty()) {
				return com.badlogic.gdx.utils.Base64Coder.decodeString(s);
			}
		}
		return "";
	}

	@Override
	public void dispose() {
		playerTexture.dispose();

		skin.dispose();
		stage.dispose();
		player2LT.dispose();
		player1LT.dispose();
		// bonusTexture.dispose();
		bonusLifeTexture.dispose();
		playerLoseT.dispose();
		blocTex.dispose();

		pauseButTex.dispose();
		newPlayerTex.dispose();
		newPlayBar.dispose();

		Gdx.app.log("gameScreenCass", "Disposed");

	}

	public ArrayList<Brick> getBrick() {
		return brick;
	}

	public void setBrick(ArrayList<Brick> brick) {
		this.brick = brick;
	}
}
