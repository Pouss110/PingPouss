package com.monnoye.game.PingPouss.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.monnoye.game.PingPouss.Assets;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import com.monnoye.game.PingPouss.PingPouss;
import com.monnoye.game.PingPouss.Object.Brick;

public class gameWhackemolCass implements Screen {
	float w, h, xT, yT;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;
	Brick b;
	private static Stage stage;
	static Skin skin;
	
	
	public gameWhackemolCass(final PingPouss game){
		this.game=game;
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();
		
		stage = new Stage();
		skin = Assets.skin;

		camera = new OrthographicCamera();
		camera.setToOrtho(true, w, h);
		camera.update();
		
		batch = game.getBatch();
		
		
		AddnewLevel();
		
	}
	
	
	private void AddnewLevel() {
		b = new Brick(50, 50, 1, 1);
		  MoveToAction moveAction = new MoveToAction();
	        moveAction.setPosition(300f, 0f);
	        moveAction.setDuration(10f);
	        b.addAction(moveAction);
		stage.addActor(b);
	}


	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		stage.act(Gdx.graphics.getDeltaTime());
		stage.act();
		stage.draw();
		
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	

	
}

