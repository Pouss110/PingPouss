package com.monnoye.game.PingPouss.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.monnoye.game.PingPouss.Assets;
import com.monnoye.game.PingPouss.PingPouss;
import com.monnoye.game.PingPouss.Logic.Profile;

public class gameScoreScreen implements Screen {
	float w, h, xT, yT;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;
	BitmapFont font;
	private static Stage stage;
	static Skin skin;
	NinePatch buttonTex;
	int levelmax;
	public static  Table scrollTable;
	public gameScoreScreen(final PingPouss game) {
		this.game = game;
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		stage = new Stage();
		skin = Assets.skin;
		

	
		  scrollTable = new Table();
		//scrollTable.setFillParent(true);
		final TextButton buttont = new TextButton("Retour", skin);
		buttont.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.gameLaunchScreen);
			}
		});

		
		levelmax = Profile.getLevelMax();
		for (int i = 1; i <= 3; i++) {
			final TextButton menubutton = new TextButton("Level " + i + "", skin);
			final TextButton highscore = new TextButton(""
					+ Profile.getHighScore(i) + "", skin);

			menubutton.setName("" + i + "");
			highscore.setName("hs" + i);
			scrollTable.add(menubutton).top().left().height(70);
			scrollTable.add(highscore).top().height(70).minWidth(70);
			scrollTable.row();

			scrollTable.findActor("hs" + i).getListeners().clear();
			scrollTable.findActor("" + i).getListeners().clear();

			if (i == 5 || i == 10 || i == 15 || i == 20) {
				// scrollTable.row();
			}
		}

		final ScrollPane scroller = new ScrollPane(scrollTable);

		Table table = new Table();
		table.setFillParent(true);
		table.add(buttont).top().height(70);
		table.row();
		table.add(scroller).top().fill();

		stage.addActor(table);
		table.setFillParent(true);
		table.top();
		table.debug();

		camera = new OrthographicCamera();
		camera.setToOrtho(true, w, h);
		camera.update();

		batch = game.getBatch();

	}
	
	public static void updateScore(){
		scrollTable.clear();
		for (int i = 1; i <= 3; i++) {
			final TextButton menubutton = new TextButton("Level " + i + "", skin);
			final TextButton highscore = new TextButton(""
					+ Profile.getHighScore(i) + "", skin);

			menubutton.setName("" + i + "");
			highscore.setName("hs" + i);
			scrollTable.add(menubutton).top().left().height(70);
			scrollTable.add(highscore).top().height(70).minWidth(70);
			scrollTable.row();

			scrollTable.findActor("hs" + i).getListeners().clear();
			scrollTable.findActor("" + i).getListeners().clear();

			if (i == 5 || i == 10 || i == 15 || i == 20) {
				// scrollTable.row();
			}
		}
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);



		batch.begin();
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.act();
		stage.draw();
		Table.drawDebug(stage);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		updateScore();
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
