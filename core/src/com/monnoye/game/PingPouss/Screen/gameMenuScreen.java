package com.monnoye.game.PingPouss.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.monnoye.game.PingPouss.Assets;
import com.monnoye.game.PingPouss.Logic.Profile;
import com.monnoye.game.PingPouss.PingPouss;

public class gameMenuScreen implements Screen {
	float w, h, xT, yT;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;

	private static Stage stage;
	static Skin skin;
	public static int nbrLevelTotal;

	public int levelMax;

	public gameMenuScreen(final PingPouss game) {
		this.game = game;
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		levelMax = Profile.getLevelMax();

		stage = new Stage();
		skin = Assets.skin;

		Table table = new Table();
		table.setFillParent(true);
		stage.addActor(table);

		TextButton menubut = new TextButton("Retour", skin);

		menubut.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.gameLaunchScreen);
			}
		});

		//table.debug();
		// final TextArea button = new TextArea("1", skin);

		menubut.setHeight(70);
		menubut.setPosition(w / 2 - menubut.getWidth() / 2,
				h - menubut.getHeight());
		stage.addActor(menubut);
		// table.add(menubut);

		testfile();

		for (int i = 1; i <= nbrLevelTotal; i++) {
			final TextButton menubutton = new TextButton("" + i + "", skin);
			menubutton.setName("" + i + "");
			table.add(menubutton).pad(10).height(77).width(77);
			menubutton.isPressed();

			if (i == 5 || i == 10 || i == 15 || i == 20) {
				table.row();
			}
		}

		
		for (int i = 1; i <= nbrLevelTotal; i++) {

			final int l = i;
			table.findActor("" + i + "").addListener(new ClickListener() {

				public void clicked(InputEvent event, float x, float y) {
					gameScreenCass.level = l;
					game.setScreen(game.gameScreenCass);
				}

			});
			if (i > levelMax) {

				table.findActor("" + i + "").setColor(Color.LIGHT_GRAY);
			}
		}

		for (int i = 1; i <= nbrLevelTotal; i++) {
			if (i > levelMax) {
				//table.findActor("" + i + "").getListeners().clear();
			}
		}

		camera = new OrthographicCamera();
		camera.setToOrtho(true, w, h);
		camera.update();

		batch = game.getBatch();

		// init the font

	}

	public static void testfile() {
		FileHandle file = Gdx.files.internal("data/level/");
		nbrLevelTotal = file.list().length;

		// file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s),
		// false);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		stage.act(Gdx.graphics.getDeltaTime());
		stage.act();
		stage.draw();
		Table.drawDebug(stage);

		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(null);

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public static String readFile(String fileName) {
		FileHandle file = Gdx.files.internal(fileName);
		if (file != null && file.exists()) {
			String s = file.readString();
			if (!s.isEmpty()) {
				return com.badlogic.gdx.utils.Base64Coder.decodeString(s);
			}
		}
		return "";
	}
}
