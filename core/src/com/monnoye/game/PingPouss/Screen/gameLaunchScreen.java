package com.monnoye.game.PingPouss.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.monnoye.game.PingPouss.Assets;
import com.monnoye.game.PingPouss.PingPouss;

public class gameLaunchScreen implements Screen {
	float w, h, xT, yT;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;
	BitmapFont font;
	NinePatch menuButTex, menuButDown, menuButChecked;
	Sprite buttonReset;
	private static Stage stage;
	static Skin skin;

	public gameLaunchScreen(final PingPouss game) {
		this.game = game;
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		stage = new Stage();
		skin = Assets.skin;

		Table table = new Table();
		table.setFillParent(true);
		stage.addActor(table);

		table.debug();
		final TextButton button = new TextButton("New Game", skin);
		final TextButton buttont = new TextButton("Score", skin);

		table.add(button).width(250).height(70).space(0, 0, 20, 0);
		table.row();
		table.add(buttont).width(200).height(70);

		button.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.gameMenuScreen);
			}
		});

		buttont.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.gameScoreScreen);
			}
		});

		// table.add(new Image(skin.newDrawable("white", Color.RED))).size(12);

		camera = new OrthographicCamera();
		camera.setToOrtho(true, w, h);
		camera.update();

		batch = game.getBatch();

		// init the font

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.act();
		stage.draw();
		// Table.drawDebug(stage);
		// font.draw(batch, "Recommencer ? Cliquez N'importe ou!", w/2, h/2);
		/*
		 * buttonReset.draw(batch);
		 * 
		 * if(Gdx.input.isTouched()== true){ xT=Gdx.input.getX();
		 * yT=Gdx.input.getY(); if(xT <= 290 && xT >= 190 && yT <= 450 && yT >=
		 * 350) { game.setScreen(game.gameScreenCass); }
		 * 
		 * 
		 * }
		 */
	
		
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
