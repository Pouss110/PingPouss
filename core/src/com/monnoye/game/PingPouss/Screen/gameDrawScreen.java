package com.monnoye.game.PingPouss.Screen;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

//import org.json.JSONArray;
//import org.json.JSONTokener;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;
import com.google.gson.Gson;
import com.monnoye.game.PingPouss.PingPouss;
import com.monnoye.game.PingPouss.Object.Brick;

public class gameDrawScreen implements Screen {
	float w, h, wNb, hNb, xTouch, yTouch, x,y;
	int xPos, yPos, iI, brTint, brFonce;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;
	Json json;
	//JSONArray jTokener;
	Gson gson;
	
	Rectangle brickBounds, clickBounds;
	Sprite brickSprite;
	Texture brickTexture;
	TextureRegion[][] briColTex;
	float[][] listPosition;
	
	ArrayList<Brick> brick;
	Iterator<Brick> brickIterator;
	
	Brick b;

	public gameDrawScreen(PingPouss game) {
		this.game = game;

		w = Gdx.graphics.getWidth();
		wNb = w / 30;
		h = Gdx.graphics.getHeight();
		hNb = (h-5) / 53;

		camera = new OrthographicCamera();

		camera.setToOrtho(true, w, h);
		camera.update();

		json  = new Json();
		gson = new Gson();
		
		//json.setOutputType(OutputType.json);
		
		
		batch = game.getBatch();

		brickTexture = new Texture(Gdx.files.internal("sheets/brickColored.png"));
		brickTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		briColTex = new TextureRegion(brickTexture).split(30, 15);
		brTint = 0;
		brFonce= 0;
		brickBounds = new Rectangle();
		clickBounds = new Rectangle();
		brick = new ArrayList<Brick>();
		brick.add( new Brick(15,5,0,0));
	}

	
	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);
		
		brickSprite = new Sprite(briColTex[0][0]);
		
		
		batch.begin();
		
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			
			xTouch = Gdx.input.getX();
			yTouch = Gdx.input.getY();
			
			
			xPos= MathUtils.round(((xTouch-brickSprite.getWidth()/2)/30));
			yPos= Math.round(((yTouch-brickSprite.getHeight()/2)/15));
			
			
			
			for (brickIterator = brick.iterator(); brickIterator.hasNext();){
				b = brickIterator.next();
				brickBounds.set(b.position.x - (brickSprite.getWidth() / 2),
						b.position.y - (brickSprite.getHeight() / 2),
						brickSprite.getWidth(), brickSprite.getHeight());
				
				if(Gdx.input.justTouched()){
				if(!clickBounds.overlaps(brickBounds)){
					
					brick.add(new Brick(xPos*30+15,yPos*15+5, brTint, brFonce));
					System.out.println(json.toJson(brick, Brick.class));
					break;
				}}
				
			}
			
			
			
			
				
		}
		if(Gdx.input.isButtonPressed(Input.Buttons.MIDDLE)==true){
			save();
		}
		
		if(Gdx.input.isButtonPressed(Input.Buttons.RIGHT)==true){
			
			
			xTouch = Gdx.input.getX();
			yTouch = Gdx.input.getY();
			
			clickBounds.set(xTouch, yTouch, 5, 5);
			
			
			for (brickIterator = brick.iterator(); brickIterator.hasNext();){
				b = brickIterator.next();
				brickBounds.set(b.position.x - (brickSprite.getWidth() / 2),
						b.position.y - (brickSprite.getHeight() / 2),
						brickSprite.getWidth(), brickSprite.getHeight());
				
				if(clickBounds.overlaps(brickBounds)){
					brickIterator.remove();
					break;
				}
				
			}
			}
		
		if(Gdx.input.isKeyPressed(Input.Keys.F1)){
			brTint=0;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F2)){
			brTint=1;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F3)){
			brTint=2;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F4)){
			brTint=3;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F5)){
			brTint=4;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F6)){
			brTint=5;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F7)){
			brTint=6;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F8)){
			brTint=7;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F9)){
			brTint=8;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F10)){
			brTint=9;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.F11)){
			brTint=10;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_0)){
			brFonce=0;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_1)){
			brFonce=1;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_2)){
			brFonce=2;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_3)){
			brFonce=3;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_4)){
			brFonce=4;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_5)){
			brFonce=5;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_6)){
			brFonce=6;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_7)){
			brFonce=7;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_8)){
			brFonce=8;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.NUM_9)){
			brFonce=9;
		}
		
		if(brick.isEmpty() == false){
		for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
			b = brickIterator.next();
			
			
			
			brickSprite.setRegion(briColTex[b.bricTint][b.bricFon]);
			batch.draw(brickSprite, b.position.x
					- (brickSprite.getWidth() / 2),
					b.position.y - (brickSprite.getHeight() / 2));
		}}
		
		batch.end();
	}
	public void save(){
		
			for (brickIterator = brick.iterator(); brickIterator.hasNext();) {
				b = brickIterator.next();
				
				
			}
			
		
		
			
			 
		
		
		
		System.out.println("Saved");
		

		//json.toJson(brick, Gdx.files.external("data/listbrick"));
		
		writeFile("data/level/level02.lvl", gson.toJson(brick));
	}
	public static void writeFile(String fileName, String s) {
		FileHandle file = Gdx.files.internal(fileName);
		file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s), false);
		
	}

	public static String readFile(String fileName) {
		FileHandle file = Gdx.files.local(fileName);
		if (file != null && file.exists()) {
			String s = file.readString();
			if (!s.isEmpty()) {
				return com.badlogic.gdx.utils.Base64Coder.decodeString(s);
			}
		}
		return "";
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		brickTexture.dispose();
	}

}
