package com.monnoye.game.PingPouss.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.monnoye.game.PingPouss.PingPouss;
import com.monnoye.game.PingPouss.Logic.InputHandler;
import com.monnoye.game.PingPouss.Object.Ball;
import com.monnoye.game.PingPouss.Object.Block;
import com.monnoye.game.PingPouss.Object.Player;

public class gameScreen implements Screen {
	float w, h;
	OrthographicCamera camera;
	SpriteBatch batch;
	Rectangle playerBarBounds, ordiBarBounds, ballBounds, filetBounds;
	Sprite playerSprite, ordiSprite, ballSprite;
	int pointOrdi = 0, pointPlayer = 0;
	
	
	Player player, ordi;
	Ball ball;
	Block block;
	int i = 0;
	Texture playerTexture, ordiTexture, ballTexture, filetTexture;
	PingPouss game;
	
	public gameScreen(PingPouss game) {
		this.game = game;

		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();

		camera.setToOrtho(true, w, h);
		camera.update();

		batch = game.getBatch();
		
		playerTexture = new Texture(Gdx.files.internal("sheets/barGreen.png"));
		playerTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		ordiTexture = new Texture(Gdx.files.internal("sheets/barRed.png"));
		ordiTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		ballTexture = new Texture(Gdx.files.internal("sheets/bullet.png"));
		ballTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		
		
		player = new Player(w/2, h-20);
		ordi = new Player(w/2, h-460);
		ball = new Ball(w/2, h/2);
		
		playerBarBounds = new Rectangle();
		ordiBarBounds = new Rectangle();
		ballBounds = new Rectangle();
		filetBounds = new Rectangle();
		
		Gdx.input.setInputProcessor(new InputHandler(player, ball,block));
		

	}

	@Override
	public void render(float delta) {
		// clear the screen
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		playerSprite = new Sprite(playerTexture);
		ordiSprite = new Sprite(ordiTexture);
		ballSprite = new Sprite(ballTexture);
		
		ball.update();
		player.update();
	
		// Draw stuff to the screen
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			ordiSprite.setPosition(ordi.position.x-(playerSprite.getWidth()/2), ordi.position.y-(playerSprite.getHeight()/2));
			ordiSprite.draw(batch);
			ballSprite.setPosition(ball.position.x-(ballSprite.getWidth()/2), ball.position.y-(ballSprite.getHeight()/2));
			
			ballSprite.draw(batch);
			ballBounds.set(ball.position.x, ball.position.y, ballSprite.getWidth(), ballSprite.getHeight());
			playerBarBounds.set(player.position.x-(playerSprite.getWidth()/2), player.position.y-(playerSprite.getHeight()/2), playerSprite.getWidth(), playerSprite.getHeight());
			ordiBarBounds.set(ordi.position.x-(playerSprite.getWidth()/2), ordi.position.y-(playerSprite.getHeight()/2), ordiSprite.getWidth(), ordiSprite.getHeight());
			
			if (i == 0){
				ball.velocity.set(0,4);
			}
			//rebond de la balle sur le plyer et ordi
			if (ballBounds.overlaps(playerBarBounds)){
				i = 1;
				if(ball.position.x >= player.position.x){
				ball.update(MathUtils.random(0,6), -4);
				}
				if(ball.position.x <= player.position.x){
					ball.update(MathUtils.random(-6,0), -4);
					}
			}
			if(ballBounds.overlaps(ordiBarBounds)){
				if(ball.position.x >= ordi.position.x){
					ball.update(MathUtils.random(0,6), 4);
					}
					if(ball.position.x <= ordi.position.x){
						ball.update(MathUtils.random(-6,0), 4);
						}
			}
			//Ordi suit la balle
			if(ordi.position.x < ball.position.x){
				ordi.update(3,0);
			}
			if(ordi.position.x > ball.position.x){
				ordi.update(-3,0);
			}
			if(ordi.position.x == ball.position.x){
				ordi.update(0,0);
			}
			//Rebond sur les murs
			if(ball.position.x <= 0 && ball.velocity.x <0 && ball.velocity.y == -4) {
				ball.update(MathUtils.random(1, 3), -4);
			}
			if(ball.position.x <= 0 && ball.velocity.x <0 && ball.velocity.y == 4) {
				ball.update(MathUtils.random(1, 3), 4);
			}
			if(ball.position.x <= 0 && ball.velocity.x >0 && ball.velocity.y == -4) {
				ball.update(MathUtils.random(1, 3), -4);
			}
			if(ball.position.x <= 0 && ball.velocity.x >0 && ball.velocity.y ==  4) {
				ball.update(MathUtils.random(1, 3), 4);
			}
			if(ball.position.x >= 640 && ball.velocity.x <0 && ball.velocity.y == -4) {
				ball.update(MathUtils.random(1, 3), -4);
			}
			if(ball.position.x >= 640 && ball.velocity.x <0 && ball.velocity.y == 4) {
				ball.update(MathUtils.random(1, 3), 4);
			}
			if(ball.position.x >= 640 && ball.velocity.x >0 && ball.velocity.y == -4) {
				ball.update(MathUtils.random(-3, -1), -4);
			}
			if(ball.position.x >= 640 && ball.velocity.x >0 && ball.velocity.y ==  4) {
				ball.update(MathUtils.random(-3, -1), 4);
			}
			//ball Out
			if(ball.position.y >= 480){
				++pointPlayer;
				ball.position.set(320, 240);
				ball.velocity.set(0,-4);
			}
			if(ball.position.y <=0){
				++pointOrdi;
				ball.position.set(320, 240);
				ball.velocity.set(0,4);
			}
				
			playerSprite.setPosition(player.position.x-(playerSprite.getWidth()/2), player.position.y-(playerSprite.getHeight()/2));
			playerSprite.draw(batch);
		batch.end();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		Gdx.app.log("GameScreen", "Disposed");
	}

}
