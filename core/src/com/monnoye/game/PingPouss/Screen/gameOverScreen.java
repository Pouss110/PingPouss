package com.monnoye.game.PingPouss.Screen;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.monnoye.game.PingPouss.Assets;
import com.monnoye.game.PingPouss.PingPouss;

public class gameOverScreen implements Screen{
	float w, h, xT, yT, bombX, bombY;
	OrthographicCamera camera;
	SpriteBatch batch;
	PingPouss game;

	Skin skin;

	float temps, tempsD;

	
	
	public gameOverScreen(PingPouss game){
		this.game = game;
		w=Gdx.graphics.getWidth();
		h=Gdx.graphics.getHeight();
	
		
		
		
		

		
		camera = new OrthographicCamera();
		camera.setToOrtho(true, w, h);
		camera.update();
		
		batch = game.getBatch();
		
		
		

		
		 temps=0;
		Assets.load();
		
		
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
	
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		


		
        
		
		
	
		
		batch.begin();
		System.out.println(temps);
		Assets.font.setScale(1f, -1f);
		Assets.font.draw(batch, "Recommencer ? Cliquez N'importe ou!", w/4, h/2);
		Assets.resetScore();

		if(Gdx.input.isTouched()== true){
			xT=Gdx.input.getX();
			yT=Gdx.input.getY();
			
			//if(xT <= 300 && xT >= 180 && yT <= 460 && yT >= 340) {
				//gameScreenCass.nbrLife=3;
				game.setScreen(game.gameScreenCass);
			//}
			
			
		}
	
		
		batch.end();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
