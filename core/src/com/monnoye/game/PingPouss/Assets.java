package com.monnoye.game.PingPouss;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.monnoye.game.PingPouss.Screen.gameScreenCass;

public class Assets {

	public static BitmapFont font, shadow;
	public static Texture ballTex, brickTex, briColTex, butMenuTex,
			explosionTex, lifeTex ;
	public static TextureRegion ex_frame_01, ex_frame_02, ex_frame_03,
			ex_frame_04, ex_frame_05, ex_frame_06, life, looselife;
	public static NinePatch butMenuNinePatch, menuButDown;
	public static TextureRegion[][] brickReg;
	public static TextureRegion[] bombeAnimRegion;
	public static Animation bombeAnimation;
	public static Skin skin;

	public static void load() {
		brickTex = new Texture(Gdx.files.internal("sheets/brickBasBW.png"));
		
		brickTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		ballTex = new Texture(Gdx.files.internal("sheets/bullet.png"));
		ballTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		// for life
		lifeTex = new Texture(Gdx.files.internal("sheets/life.png"));
		lifeTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		life = new TextureRegion(lifeTex,0,32,32,32);
		life.flip(false, true);
		looselife = new TextureRegion(lifeTex,0,0,32,32);
		looselife.flip(false, true);
		//for brick
		briColTex = new Texture(Gdx.files.internal("sheets/brickColored.png"));
		briColTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		brickReg = new TextureRegion(briColTex).split(30, 15);

		// init the font
		font = new BitmapFont(Gdx.files.internal("fonts/ping_text-export.fnt"),
				Gdx.files.internal("fonts/ping_text-export.png"), false);
		font.setScale(1f, 1f);
		// Animation des bombes;

		
		
		// Texture for skin
		butMenuTex = new Texture(Gdx.files.internal("sheets/buttonMenu.png"));
		briColTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		menuButDown = new NinePatch(butMenuTex, 12, 12, 12, 12);
		menuButDown.setColor(Color.LIGHT_GRAY);
		butMenuNinePatch = new NinePatch(butMenuTex, 12, 12, 12, 12);

		// Skin Load
		skin = new Skin();
		skin.add("buttonImg", butMenuNinePatch);
		skin.add("buttonImgOver", menuButDown);
		skin.add("default", font);

		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));

		TextButtonStyle tbs = new TextButtonStyle();
		
		tbs.up = skin.getDrawable("buttonImg");
		tbs.down = skin.getDrawable("buttonImg");
		// tbs.checked = skin.getDrawable("buttonImg");
		tbs.over = skin.getDrawable("buttonImgOver");
		tbs.font = skin.getFont("default");
		tbs.unpressedOffsetY = 20;
		tbs.pressedOffsetY = 20;
		skin.add("default", tbs);

		WindowStyle ws = new WindowStyle();
		ws.background = skin.getDrawable("buttonImg");
		ws.titleFont = skin.getFont("default");
		ws.stageBackground = skin.newDrawable("white", 0.2f, 0.2f, 0.2f, 0.5f);
		skin.add("default", ws);
	}

	public static void dispose() {
		ballTex.dispose();
		brickTex.dispose();
		lifeTex.dispose();
		font.dispose();
		briColTex.dispose();
		butMenuTex.dispose();
		skin.dispose();

	}

	public static void resetScore() {
		gameScreenCass.score = 0;
		gameScreenCass.strScore = new String()
				.valueOf(gameScreenCass.score * 10);
		gameScreenCass.scoretext.setText(gameScreenCass.strScore);
		gameScreenCass.scoreBar = 0;
		gameScreenCass.pBar.setValue(gameScreenCass.scoreBar);
		gameScreenCass.i = 0;
		gameScreenCass.ballStop = true;

	}

}
