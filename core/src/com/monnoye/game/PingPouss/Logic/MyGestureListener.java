package com.monnoye.game.PingPouss.Logic;

import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.monnoye.game.PingPouss.Object.Ball;
import com.monnoye.game.PingPouss.Screen.gameScreenCass;

public class MyGestureListener implements GestureListener{
	public static float xTap, yTap;
	 Ball	ball;
	 
	public MyGestureListener(Ball ball){
		this.ball = ball;
	}
	@Override
    public boolean touchDown(float x, float y, int pointer, int button) {

        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
    			gameScreenCass.tap = true;
    			if(gameScreenCass.mode==1){
    			xTap=x;
    			yTap=y;
    			 	}
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {

        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
    	if(gameScreenCass.ballStop == true){
    		
    		ball.velocity.x = velocityX/400;
    		ball.velocity.y = velocityY/400;
    		gameScreenCass.ballStop = false;
    	}
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {

        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {

        return false;
    }

    @Override
    public boolean zoom (float originalDistance, float currentDistance){

       return false;
    }

    @Override
    public boolean pinch (Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer){

       return false;
    }
}