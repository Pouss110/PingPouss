package com.monnoye.game.PingPouss.Logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.monnoye.game.PingPouss.Object.Ball;
import com.monnoye.game.PingPouss.Object.Block;
import com.monnoye.game.PingPouss.Object.Bomb;
import com.monnoye.game.PingPouss.Object.Player;
import com.monnoye.game.PingPouss.Screen.gameScreenCass;
import com.monnoye.game.PingPouss.Screen.gameWhackemolCass;

public class InputHandler implements InputProcessor {
		 Player player;
		  Ball	ball;
		  Block block;
		  ImageButton pauseBu;
		
		private static float xTouch,xTouchD;
		private static float yTouch, yTouchD;
		
		public InputHandler(Player player, Ball ball, Block block) {
			this.player = player;
			this.ball = ball;
			this.block = block;
		}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode){
		
		case Keys.Q:
			player.velocity.x = -3;
			break;
		
		case Keys.D:
			player.velocity.x = 3;
			break;
		case Keys.SPACE:
			if(gameScreenCass.ballStop == true){
			gameScreenCass.ballStop = false;}else{}
			break;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		
		
		case Keys.Q:
			if (player.velocity.x < 0) player.velocity.x = 0;
			break;
		case Keys.D:
			if (player.velocity.x > 0) player.velocity.x = 0;
			break;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		xTouchD = Gdx.input.getX();
		yTouchD = Gdx.input.getY();
		gameScreenCass.touchUp= false;
		
		if(gameScreenCass.ballStop == false){
		gameScreenCass.debutVec = new Vector2(xTouchD, yTouchD);
		}
	/*	if(yTouch <= Gdx.graphics.getHeight()/2){
			if(gameScreenCass.ballStop == true){
				
				if((xTouchD/Gdx.graphics.getWidth())>0.5){
				ball.velocity.x=(xTouchD/Gdx.graphics.getWidth())*2.5f;
				
				ball.velocity.y=-3;}else{
					ball.velocity.x=((xTouchD-Gdx.graphics.getWidth())/Gdx.graphics.getWidth())*2.5f;
					
					ball.velocity.y=-3;//((yTouchD-yTouchD-yTouchD)/Gdx.graphics.getHeight())*2.5f;
				}
				System.out.print(xTouchD);
				gameScreenCass.ballStop = false;}else{}
			
		}
		*/
		
		if(xTouchD > player.position.x ){
		
					
		}else if(xTouchD < player.position.x){
			
			}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		player.velocity.x=0;
		
		xTouch = Gdx.input.getX();
		yTouch = Gdx.input.getY();		
		
		if(gameScreenCass.ballStop == false){
		gameScreenCass.finVec = new Vector2(xTouch, yTouch);
		}
		gameScreenCass.touchUp = true;
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		
		
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
