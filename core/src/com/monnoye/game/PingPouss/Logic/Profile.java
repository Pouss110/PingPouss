package com.monnoye.game.PingPouss.Logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;



public class Profile{
	
	
	
	public static Preferences prefs;
	 
	 
	public static void load() {
		prefs =  Gdx.app.getPreferences("PingPouss");
		if (!prefs.contains("highScore1")) {
	        prefs.putLong("highScore1", 0);
	      //prefs.flush();
	    }
		if (!prefs.contains("level")) {
	        prefs.putInteger("level", 1);
	        
	    }
		if (!prefs.contains("longBarMax")){
			prefs.putInteger("longBarMax", -120);
		}
		if(!prefs.contains("credit")){
			prefs.putLong("credit", 0);
		}
	//	if(!prefs.contains(""))
		
		//currentLevelID = 1;
		//writeFile("data/playerLvl.pfr", gson.toJson(currentLevelID));
	}
	
		
	
	  // Create (or retrieve existing) preferences file
  
public static void setCredit(long val){
	prefs.putLong("credit", val);
	prefs.flush();
}
   
public static long getCredit(){
	return prefs.getLong("credit");
}
public static void setLevelMax(int val){
	prefs.putInteger("level", val);
	prefs.flush();
}
public static int getLevelMax(){
	return prefs.getInteger("level");
}
	
public static void setHighScore(long val, int key) {
    prefs.putLong("highScore"+key, val);
    prefs.flush();
}

public static long getHighScore(int key) {
    return prefs.getLong("highScore"+key);
}
	
public static void setLongMax(int val){
	prefs.putInteger("longBarMax", val);
	prefs.flush();
}
public static int getLongMax(){
	return prefs.getInteger("longBarMax");
}
	

	public static void testfile(String fileName, String s) {
		FileHandle file = Gdx.files.internal("data/");
		int i =file.list().length;
		 
		//file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s), false);
		
	}
	
	public static void writeFile(String fileName, String s) {
		FileHandle file = Gdx.files.internal(fileName);
		file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s), false);
		
	}
	
	public static String readFile(String fileName) {
		FileHandle file = Gdx.files.internal(fileName);
		if (file != null && file.exists()) {
			String s = file.readString();
			if (!s.isEmpty()) {
				return com.badlogic.gdx.utils.Base64Coder.decodeString(s);
			}
		}
		return "";
	}

	
}
